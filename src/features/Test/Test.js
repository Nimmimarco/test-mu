import React, { PureComponent } from "react";

class Test extends PureComponent {
  data = [
    "asd",
    {
      name: "MobileUp",
      addresses: [
        {
          address: "ул. Большая Московская, д.1",
          asdasd: [
            {
              kdsd: "asdas",
              asdasd: "wwww"
            }
          ],
          services: [
            { name: "internet", price: 1000 },
            { name: "ip address", price: 200 }
          ]
        }
      ]
    },
    {
      name: "MobileUp",
      sssad: "asdas",
      addresses: [
        {
          asdasdasss: "ssss",
          address: "ул. Большая Московская, д.1",
          services: [
            { name: "internet", price: 1000 },
            { name: "ip address", price: 200 }
          ]
        }
      ]
    }
  ];

  componentDidMount() {
    [...document.querySelectorAll("tr td")]
      .map((el, idx) => {
        return `tr td:nth-child(${idx - 1}), tr td:nth-child(${idx + 1})`;
      })
      .forEach(_el => {
        let a;
        let elements = document.querySelectorAll(_el);
        elements.forEach((b, c) => {
          a && a.textContent === b.textContent
            ? (b.parentNode.removeChild(b), a.rowSpan++)
            : (a = b);
        });
      });

    const a = new Promise((resolve, rej) => setTimeout(() => resolve(2), 1000));
    const b = new Promise((resolve, rej) => setTimeout(() => resolve(5), 5000));
    //const c = new Promise((resolve, rej) => resolve(6))

    const race = promises => {
      return new Promise((resolve, reject) => {
        promises.forEach(promise => {
          promise
            .then(value => {
              resolve(value);
            })
            .catch(reject);
        });
      });
    };

    console.log("race", race([a, b]).then(res => console.log("res", res)));
  }

  render() {
    let markup = "";

    const traverse = (_data, keys = []) => {
      _data.forEach((d, idx) => {
        if (typeof d === "object" && !d.forEach) {
          for (let key in d) {
            if (!!d[key].forEach) {
              traverse(d[key], [...keys, key]);
            } else {
              markup += `<tr>
              ${keys.reduce((acc, k, _idx) => (acc += `<td>${k}</td>`), "")}
              <td>${d[key]}</td>
              </tr>`;
            }
          }
        } else {
          markup += `<tr>
              <td>${d}</td>
              </tr>`;
        }
      });
    };

    traverse(this.data);

    return (
      <table
        dangerouslySetInnerHTML={{ __html: markup }}
        width="100%"
        border="1"
        align="center"
        cellPadding="5"
        cellSpacing="0"
      />
    );
  }
}

export default Test;
