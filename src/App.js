import React, { PureComponent } from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Test } from "./features/Test";
import { withStyles } from "@material-ui/core/styles";

import Paper from "@material-ui/core/Paper";

const styles = theme => ({
  "@global": {
    body: {
      margin: 0,
      padding: 0,
      fontSize: "15px"
    },
    ul: {
      padding: 0,
      margin: 0
    },
    li: {
      listStyleType: "none"
    },
    a: {
      color: "inherit",
      textDecoration: "none"
    }
  },
  app: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  root: theme.mixins.gutters({
    width: "40%",
    height: "80vh",
    overflow: "auto",
    padding: theme.spacing.unit * 3
  })
});

class App extends PureComponent {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.app}>
        <Paper className={classes.root} elevation={15}>
          <Test />
        </Paper>
      </div>
    );
  }
}

export const AppContainer = withStyles(styles)(App);

export const renderApp = (ClientAppComponent, store, DOMElement) => {
  const component = (
    <Provider store={store}>
      <ClientAppComponent />
    </Provider>
  );

  render(component, DOMElement);
};
